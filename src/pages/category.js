import React, {useState,useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

toast.configure()
const Category = () => {

  const[catgs,getCategory]=useState([])
  const[count,setCount]=useState(0);
  
  const handleDelete=(id)=>{
    setCount(count+1)
    axios.get(`http://localhost/React/webApp/cms/save_data.php?actionVal=DelCatg&delid=${id}`)
    .then(res=>{
      toast.success("Deleted Success",{position:toast.POSITION.TOP_CENTER,autoClose:8000})
    })
    .catch(err=>{
        console.log(err)
    })
  }
useEffect(()=>{
    axios.get(`http://localhost/React/webApp/cms/save_data.php?actionVal=getCategory`)
    .then(res=>{
        if(res.data!=='no data')
        {
        console.log(res.data,"CategoryList")
        getCategory(res.data)
        }else{
            console.log(res,"No data")
            getCategory([])
        }
    })
    .catch(err=>{
        getCategory([{}])
        console.log(err,"Errors")
    })
},[count])

    return ( 
      
                <div className="content-wrapper">
        {/* Content Header (Page header) */}
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0 text-dark">Category</h1>
              </div>{/* /.col */}
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="">Home</Link></li>
                  <li className="breadcrumb-item active">Dashboard v1</li>
                </ol>
              </div>{/* /.col */}
            </div>{/* /.row */}
          </div>{/* /.container-fluid */}
        </div>
        {/* /.content-header */}

            {/* Main content */}
    <section className="content">
      <div className="container-fluid">
        <div className="row">
          {/* left column */}
          <div className="col-md-12">
            {/* general form elements */}
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Quick Example</h3>
                <Link to="/addCategory" className="btn btn-danger float-right"> Add</Link>
              </div>
              {/* /.card-header */}
              {/* form start */}
       
              
                <div className="card-body">
                <table id="example1" className="table table-bordered table-hover">
                <thead>
                <tr>
                <th>#</th>
                <th> Category</th>
                <th>Description</th>
                <th>Posting Date</th>
                <th>Last updation Date</th>
                <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {
    
    catgs.map(catg=><tr key={catg.id}>
     
      
    <td>{catg.id}</td>
    <td>{catg.CategoryName}</td>
    <td>{catg.Description}</td>
    <td>{catg.PostingDate}</td>
    <td>{catg.UpdationDate}</td>
    <td><Link to={`/editCategory/${catg.id}`}><i className="fa fa-pencil" style={{color: "#29b6f6"}}></i></Link> 
	&nbsp; <i className="fa fa-trash-o" onClick={()=>handleDelete(catg.id)}   style={{color: "#f05050",cursor:"pointer"}}></i></td>
    </tr>
    )
    
    }
                </tbody>
                </table>
                </div>
                {/* /.card-body */}

                <div className="card-footer">
                 
                </div>
              
            </div>
            {/* /.card */}
            </div>
            </div>
          
        </div>
        </section>
        </div>
        
     );
}
 
export default Category;