import React, { useEffect,useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


toast.configure()
const AddCategory = ({match}) => {
  var edit_id = match.params.id ? match.params.id : ''
  const[catgs,fillCategs]=useState({
      catgname:'',
      catgdesc:''
  }) 
  const[errors,setErrors]=useState({nameErr:'',descErr:''})
  useEffect(()=>{
      axios.get(`http://localhost/React/webApp/cms/save_data.php?actionVal=EditCategory&edit_id=${edit_id}`)
      .then(res=>{
          console.log(res.data[0],"EditCategory")
          fillCategs({
              catgname: res.data[0].CategoryName,
              catgdesc: res.data[0].Description
          })
      })
      .catch(err=>{
          fillCategs({})
      })
  },[edit_id])

  const validate = () => {
    let nameError = "";
    let descError = "";
    // let passwordError = "";

    if (!catgs.catgname) {
      nameError = "name cannot be blank";
    }

    if (!catgs.catgdesc) {
      descError = "desc cannot be blank";
    }

    if (descError || nameError) {
      setErrors({ nameErr:nameError, descErr:descError });
      return false;
    }

    return true;
  };
  
  const handleCatgSubmit=(event)=>{
      event.preventDefault();
      const isValid = validate();
    if (isValid) {
      setErrors('');
      let formData = new FormData();
      formData.append('name', catgs.catgname)
      formData.append('desc', catgs.catgdesc)
      axios({
          method: 'post',
          url: `http://localhost/React/webApp/cms/save_data.php?actionVal=AddCategory&edit_id=${edit_id}`,
          data: formData,
          config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then(function (response) {
          console.log(response,"response");
          if(edit_id===''){
          toast.success("Add Success",{position:toast.POSITION.TOP_CENTER,autoClose:8000})
          }else{
            toast.success("Update Success",{position:toast.POSITION.TOP_CENTER,autoClose:8000}) 
          }
                
      })
      .catch(function (response) {
          //handle error
          //console.log(response)
      });
    }
  }
    return ( 
      
                <div className="content-wrapper">
        {/* Content Header (Page header) */}
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0 text-dark">Category</h1>
              </div>{/* /.col */}
              <div className="col-sm-6">
                <ol className="breadcrumb float-sm-right">
                  <li className="breadcrumb-item"><Link to="">Home</Link></li>
                  <li className="breadcrumb-item active">Dashboard v1</li>
                </ol>
              </div>{/* /.col */}
            </div>{/* /.row */}
          </div>{/* /.container-fluid */}
        </div>
        {/* /.content-header */}

            {/* Main content */}
    <section className="content">
      <div className="container-fluid">
        <div className="row">
          {/* left column */}
          <div className="col-md-12">
            {/* general form elements */}
            <div className="card card-primary">
              <div className="card-header">
                <h3 className="card-title">Quick Example</h3>
              </div>
              {/* /.card-header */}
              {/* form start */}
              
              <form name="category" method="post">
                <div className="card-body">
                  <div className="form-group">
                    <label>Category Name</label>
                    <input type="text" className="form-control" 
                    value={catgs.catgname} onChange={e=>fillCategs({...catgs,catgname:e.target.value})} 
                    name="category"/>
                    <div style={{ fontSize: 12, color: "red" }}>
            {errors.nameErr}
          </div>
                  </div>
                  <div className="form-group">
                    <label>Category Description</label>
                    <textarea className="form-control" rows="5" onChange={e=>fillCategs({...catgs,catgdesc:e.target.value})}  
                    value={catgs.catgdesc}  name="description" />
                   <div style={{ fontSize: 12, color: "red" }}>
            {errors.descErr}
          </div>
                  </div>
                  {/* <div className="form-group">
                    <label>Category Name:</label>
                  
                    />
                     <ErrorMessage name='active'/>
                  </div> */}
                </div>
                {/* /.card-body */}

                <div className="card-footer">
                  <button type="submit" onClick={handleCatgSubmit} className="btn btn-primary">Submit</button>
                </div>
             </form>
            </div>
            {/* /.card */}
            </div>
            </div>
          
        </div>
        </section>
        </div>
        
     );
}
 
export default AddCategory;