import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = () => {
    return (  
        <aside className="main-sidebar sidebar-dark-primary elevation-4">
          {/* hii */}
     {/* Brand Logo  */}
    <Link to="/" className="brand-link">
      <img src={process.env.PUBLIC_URL + `dist/img/AdminLTELogo.png`} alt="AdminLTE Logo" className="brand-image img-circle elevation-3"
           />
      <span className="brand-text font-weight-light">AdminLTE 3</span>
    </Link>

    {/* Sidebar */}
    <div className="sidebar">
      {/* Sidebar user panel (optional) */}
      <div className="user-panel mt-3 pb-3 mb-3 d-flex">
        <div className="image">
          <img src={process.env.PUBLIC_URL + `dist/img/user2-160x160.jpg`} className="img-circle elevation-2" alt="UserImage"/>
        </div>
        <div className="info">
          <Link to="" className="d-block">Alexander Pierce</Link>
        </div>
      </div>

      {/* Sidebar Menu */}
      <nav className="mt-2">
        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          {/* Add icons to the links using the .nav-icon className
               with font-awesome or any other icon font library */}
          <li className="nav-item has-treeview menu-open">
            <Link to="" className="nav-link active">
              <i className="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Master
                <i className="right fas fa-angle-left"></i>
              </p>
            </Link>
            <ul className="nav nav-treeview">
              <li className="nav-item">
                <Link to="/category" className="nav-link active">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Category</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </Link>
              </li>
            </ul>
          </li>
          <li className="nav-item">
            <Link to="" className="nav-link">
              <i className="nav-icon fas fa-th"></i>
              <p>
                Widgets
                <span className="right badge badge-danger">New</span>
              </p>
            </Link>
          </li>
          <li className="nav-item has-treeview">
            <Link to="" className="nav-link">
              <i className="nav-icon fas fa-copy"></i>
              <p>
                Layout Options
                <i className="fas fa-angle-left right"></i>
                <span className="badge badge-info right">6</span>
              </p>
            </Link>
            <ul className="nav nav-treeview">
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Top Navigation</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Top Navigation + Sidebar</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Boxed</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Fixed Sidebar</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Fixed Navbar</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Fixed Footer</p>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="" className="nav-link">
                  <i className="far fa-circle nav-icon"></i>
                  <p>Collapsed Sidebar</p>
                </Link>
              </li>
            </ul>
          </li>
          
        </ul>
      </nav>
      {/* /.sidebar-menu */}
    </div>
    {/* /.sidebar */}
  </aside>
    );
}
 
export default Sidebar;