import React,{useState} from 'react';
import Navbar from './navbar';
import Sidebar from './sidebar';
import Content from './content';
import {BrowserRouter as Router,Switch,Route} from  'react-router-dom';
import Footer from './footer';
import addCategory from './addCategory';
import Category from './category';
import Register from './register';
import Login from './login';


function Dashboard() {
  const [login, setlogin] = useState(true)
  return (
    <div className="wrapper">

       {login?<Router>
     <Navbar/>
     <Sidebar/>
     <Switch>
<Route exact path='/' component={Content} />
<Route exact path='/addCategory' component={addCategory} />
<Route exact path='/editCategory/:id' component={addCategory} />
<Route exact path='/category' component={Category} />
</Switch>
<Footer/>
</Router> :
<Router>
<Route exact path='/' component={Login} />
<Route exact path='/register' component={Register} />
</Router>
}

    </div>
  );
}

export default Dashboard;
