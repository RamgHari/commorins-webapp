import React from 'react';
import { Link } from 'react-router-dom';
const Footer = () => {
    return (
        <div>
        <footer className="main-footer">
    <strong>Copyright &copy; 2014-2019 <Link to="">AdminLTE.io</Link>.</strong>
    All rights reserved.
    <div className="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.5
    </div>
  </footer>

  <aside className="control-sidebar control-sidebar-dark">
  
  </aside>
  </div>

      );
}
 
export default Footer;